# ARQMath Task1: BetterThanG

Authors:
* Matteo Brosolo
* Leonardo Gemin
* Gregorio Morin
* Nicola Prevedello
* Diego Saporito

---

## Project organization
```
- seupd2021-btg
    |
    |- src
        |
        |- main
            |
            |- java
                |
                |- it/unipd/dei/se
                    |
                    |- index
                        |- AnalyzerUtil
                        |- BodyField
                        |- BtGAnalyzer
                        |- DirectoryIndexer
                        |- FormulaField
                        |- TagsField
                        |- TitleField
                    |
                    |- parse
                        |- CollectionParser
                        |- DocumentParser
                        |- ParsedDocument
                        |- TopicParser
                    |
                    |- search
                        |- Searcher
                    |
                    |- utility
                        |- Aligner
                        |- Combiner
                        |- CombinerNorm
                        |- NormalizeScore
                        |- TangentSToTrecEval
                    |
                    |- BetterThanG_ARQMath1
            |
            |- resources
                |- rouge.txt
    |
    |- python
        |- tsv2xml.py
        |- Topics_2021_Formulas_Latex_V1.1.tsv
        |- Topics_2021_Formulas_OPT_V1.1.tsv
        |- Topics_2021_Formulas_SLT_V1.1.tsv
        |- opt_task1.xml
        |- slt_task1.xml
    |
    |- README.md
```
