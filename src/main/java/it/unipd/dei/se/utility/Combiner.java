package it.unipd.dei.se.utility;

import java.io.*;
import java.util.HashSet;

public class Combiner {

    public static void main(String[] args) throws Exception {

        final String indexAPath = "../arqmathTrecFormat_ALIGNED.txt";
        final String indexBPath = "../seupd2021-btg-first-run.txt";
        final String nomeRun = "seupd2021-btg-combinedResults-run.txt";
        final String outputFile = "../" + nomeRun;

        BufferedReader a = new BufferedReader(new InputStreamReader(new FileInputStream(indexAPath)));
        BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(indexBPath)));
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));

        //----------------------------------SETTAGGIO COUNTER----------------------------------
        int numA = 2;
        int numB = 1;
        //-------------------------------------------------------------------------------------

        int tempA = numA;
        int tempB = numB;
        boolean flagA = false;
        boolean flagB = false;
        boolean doneA = false;
        boolean endA = false;
        int lineCounter = 1;
        int lineCountA = 0;
        int lineCountB = 0;
        int collision = 0;
        boolean stop = false;


        for (int i = 0; i < 100; i++) {
            HashSet<Integer> docID = new HashSet<>();
            for (int j = 0; j < 1000; j++) {
                String line = "";
                if ((!flagA) && (!endA)) {
                    String nla = a.readLine();
                    if (nla == null) {
                        stop = true;
                        break;
                    }
                    String[] tokensA = nla.split("\t");
                    if (tokensA.length < 6) {
                        endA = true;
                        System.out.println("AAAAAAAAAAAAAA" + tokensA + "AAAAAAAAAAAAAAA");
                    }

                    lineCountA++;

                    if (!endA) {
                        tempA--;
                        if (tempA == 0) {
                            flagA = true;
                            tempA = numA;
                        }

                        if (!docID.isEmpty()) {
                            while (docID.contains(Integer.parseInt(tokensA[2]))) {
                                collision++;
                                nla = a.readLine();
                                tokensA = nla.split("\t");
                                lineCountA++;
                            }
                        }

                        docID.add(Integer.parseInt(tokensA[2]));
                        line = tokensA[0] + "\t" + tokensA[1] + "\t" + tokensA[2] + "\t" + lineCounter + "\t" + tokensA[4] + "\t" + nomeRun + "\n";
                        lineCounter++;
                        doneA = true;
                    }
                }
                if ((!flagB) && (!doneA)) {
                    String nlb = b.readLine();
                    if (nlb == null) {
                        stop = true;
                        break;
                    }
                    String[] tokensB = nlb.split("\t");
                    lineCountB++;


                    tempB--;
                    if (tempB == 0) {
                        flagB = true;
                        tempB = numB;
                    }

                    if (!docID.isEmpty()) {
                        while (docID.contains(Integer.parseInt(tokensB[2]))) {
                            collision++;
                            nlb = b.readLine();
                            tokensB = nlb.split("\t");
                            lineCountB++;
                        }
                    }

                    docID.add(Integer.parseInt(tokensB[2]));
                    line = tokensB[0] + "\t" + tokensB[1] + "\t" + tokensB[2] + "\t" + lineCounter + "\t" + tokensB[4] + "\t" + nomeRun + "\n";
                    lineCounter++;


                }
                //Reset dei turni
                if (endA && flagB) {
                    flagB = false;
                }
                if (flagA & flagB) {
                    flagA = false;
                    flagB = false;
                }

                w.write(line);
                System.out.println(line);
                doneA = false;
            }

            if (stop)
                break;

            while (lineCountA < 1000) {
                String temp = a.readLine();
                lineCountA++;
            }
            while (lineCountB < 1000) {
                String temp = b.readLine();
                lineCountB++;
            }
            lineCountA = 0;
            lineCountB = 0;
            lineCounter = 1;
            flagA = false;
            flagB = false;
            endA = false;
        }

        System.out.println("Collision number: " + collision);


        w.flush();
        w.close();
        a.close();
        b.close();
    }
}
