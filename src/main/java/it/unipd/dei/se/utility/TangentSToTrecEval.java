package it.unipd.dei.se.utility;

import java.io.*;

public class TangentSToTrecEval {

    public static void main(String[] args) throws Exception {

        final String indexPath = "../arqmath-format-results.tsv";
        final String outputFile = "../arqmathTrecFormat.txt";

        BufferedReader a = new BufferedReader(new InputStreamReader(new FileInputStream(indexPath)));
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));


        while (true) {
            String line = a.readLine();
            if (line == null)
                break;
            String[] tokens = line.split("\t");
            String newLine = tokens[0] + "\t" + tokens[1] + "\t" + (Integer.parseInt(tokens[2]) - 1) + "\t" + tokens[3] + "\t" + tokens[4] +  "\t" + tokens[5] +"\n";
            w.write(newLine);
        }


        w.flush();
        w.close();
        a.close();
    }
}
