package it.unipd.dei.se.utility;

import java.io.*;

public class Aligner {

    public static void main(String[] args) throws Exception {

        final String inputPath = "../arqmathTrecFormatNormalized.txt";
        final String outputFile = "../arqmathTrecFormatNormalized_ALIGNED.txt";

        BufferedReader a = new BufferedReader( new InputStreamReader( new FileInputStream(inputPath) ) );
        BufferedWriter w = new BufferedWriter( new OutputStreamWriter( new FileOutputStream(outputFile) ) );

        int topicNumber = 1;
        int currentTopic;
        int topicLines = 0;
        boolean firstLine = true;

        while(true)
        {
            String line = a.readLine();
            if(line==null)
                break;
            String[] tokens = line.split("\t");

            if(firstLine)
            {
                String[] topicID = tokens[0].split("\\.");
                topicNumber = Integer.parseInt(topicID[1]);
                firstLine = false;
            }

            String[] topicID = tokens[0].split("\\.");
            currentTopic = Integer.parseInt(topicID[1]);

            do{
                if(currentTopic != topicNumber)
                {
                    while(topicLines<1000)
                    {
                        w.newLine();
                        topicLines++;
                    }
                    topicNumber++;
                    topicLines = 0;
                }
            }while(currentTopic != topicNumber);


            String newLine = tokens[0] + "\t" + tokens[1] + "\t" + tokens[2] + "\t" + tokens[3] + "\t" + tokens[4] + "\t" + tokens[5] + "\n";
            topicLines++;

            if(topicLines==1000)
            {
                topicNumber++;
                topicLines = 0;
            }

            w.write(newLine);
        }


        w.flush();
        w.close();
        a.close();
    }

}
