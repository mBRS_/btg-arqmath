package it.unipd.dei.se.utility;

import java.io.*;

public class NormalizeScore {

    public static void main(String[] args) throws Exception {

        final String indexPath = "../seupd2021-btg-first-run.txt";
        final String outputFile = "../seupd2021-btg-first-run-Normalized.txt";

        BufferedReader a = new BufferedReader(new InputStreamReader(new FileInputStream(indexPath)));
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));

        int docID = -1;
        int currentDocID;
        double maxScore = 1;

        while (true) {
            String line = a.readLine();
            if (line == null)
                break;
            String[] tokens = line.split("\t");

            String[] topicID = tokens[0].split("\\.");
            currentDocID = Integer.parseInt(topicID[1]);

            if(currentDocID != docID)
            {
                maxScore = Double.parseDouble(tokens[4]);
                docID = currentDocID;
            }

            String newLine = tokens[0] + "\t" + tokens[1] + "\t" + tokens[2] + "\t" + tokens[3] + "\t" + Double.parseDouble(tokens[4])/maxScore + "\t" + tokens[5] + "\n";
            w.write(newLine);
        }


        w.flush();
        w.close();
        a.close();
    }

}
