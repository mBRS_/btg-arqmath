package it.unipd.dei.se.utility;

import java.io.*;
import java.util.HashSet;

public class CombinerNorm {

    public static void main(String[] args) throws Exception {

        final String indexAPath = "../arqmathTrecFormatNormalized_ALIGNED.txt";
        final String indexBPath = "../seupd2021-btg-first-run-Normalized.txt";
        final String nomeRun = "seupd2021-btg-combinedNormResults-run.txt";
        final String outputFile = "../" + nomeRun;

        BufferedReader a = new BufferedReader(new InputStreamReader(new FileInputStream(indexAPath)));
        BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(indexBPath)));
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));


        int lineCounter = 1;
        int lineCountA = 0;
        int lineCountB = 0;
        int collision = 0;




        for (int i = 0; i < 100; i++) {
            HashSet<Integer> docID = new HashSet<>();

            String nla = a.readLine();
            String nlb = b.readLine();
            lineCountA++;
            lineCountB++;
            String[] tokensA = nla.split("\t");
            String[] tokensB = nlb.split("\t");

            for (int j = 0; j < 1000; j++) {
                String line = "";

                if (!docID.isEmpty()) {
                    while (docID.contains(Integer.parseInt(tokensA[2])) || docID.contains(Integer.parseInt(tokensB[2]))) {
                        collision++;
                        if(docID.contains(Integer.parseInt(tokensA[2])))
                        {
                            nla = a.readLine();
                            lineCountA++;
                            tokensA = nla.split("\t");
                        }
                        else
                        {
                            nlb = b.readLine();
                            lineCountB++;
                            tokensB = nlb.split("\t");
                        }
                    }
                }

                if(Double.parseDouble(tokensA[4]) >= Double.parseDouble(tokensB[4]))
                {
                    line = tokensA[0] + "\t" + tokensA[1] + "\t" + tokensA[2] + "\t" + lineCounter + "\t" + tokensA[4] + "\t" + nomeRun + "\t" +"A"+ lineCountA + "\n";
                    docID.add(Integer.parseInt(tokensA[2]));
                    nla = a.readLine();
                    lineCountA++;
                    tokensA = nla.split("\t");
                }
                else
                {
                    line = tokensB[0] + "\t" + tokensB[1] + "\t" + tokensB[2] + "\t" + lineCounter + "\t" + tokensB[4] + "\t" + nomeRun + "\t" +"B"+ lineCountB + "\n";
                    docID.add(Integer.parseInt(tokensB[2]));
                    nlb = b.readLine();
                    lineCountB++;
                    tokensB = nlb.split("\t");
                }


                w.write(line);
                lineCounter++;
                System.out.println(line);

            }

            while (lineCountA < 1000) {
                String temp = a.readLine();
                lineCountA++;
            }
            while (lineCountB < 1000) {
                String temp = b.readLine();
                lineCountB++;
            }
            lineCountA = 0;
            lineCountB = 0;
            lineCounter = 1;
        }

        System.out.println("Collision number: " + collision);


        w.flush();
        w.close();
        a.close();
        b.close();
    }

}
