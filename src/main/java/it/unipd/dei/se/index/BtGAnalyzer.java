/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se.index;


import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.*;
import org.apache.lucene.analysis.miscellaneous.TrimFilter;
import org.apache.lucene.analysis.standard.ClassicFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.*;

import static it.unipd.dei.se.index.AnalyzerUtil.loadStopList;


/**
 * Introductory example on how to use write your own {@link Analyzer} by using different {@link Tokenizer}s and {@link
 * org.apache.lucene.analysis.TokenFilter}s.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class BtGAnalyzer extends Analyzer {

	/**
	 * Creates a new instance of the analyzer.
	 */
	public BtGAnalyzer() {
		super();
	}


	@Override
	protected TokenStreamComponents createComponents(String fieldName) {

		final Tokenizer source = new StandardTokenizer();

		TokenStream tokens = new LowerCaseFilter(source);
		tokens = new TrimFilter(tokens);
		tokens = new ClassicFilter(tokens);
		tokens = new EnglishMinimalStemFilter(tokens);
		tokens = new StopFilter(tokens, loadStopList("rouge.txt"));


		return new TokenStreamComponents(source, tokens);
	}

	@Override
	protected Reader initReader(String fieldName, Reader reader) {
		return new HTMLStripCharFilter(reader);
		//return super.initReader(fieldName, reader);
	}

	@Override
	protected TokenStream normalize(String fieldName, TokenStream in) {
		return new LowerCaseFilter(in);
	}

	/**
	 * Main method of the class.
	 *
	 * @param args command line arguments.
	 *
	 * @throws IOException if something goes wrong while processing the text.
	 */
	public static void main(String[] args) throws IOException {

		// text to analyze
		final String text = "You use a proof by contradiction. Basically, you suppose that  \\sqrt{2}  can be written as  p/q . Then you know that  2q^2 = p^2 . However, both  q^2  and  p^2  have an even number of factors of two, so  2q^2  has an odd number of factors of 2, which means it can't be equal to  p^2 .";

	}

}
