/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import org.apache.lucene.benchmark.quality.QualityQuery;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TopicParser {

    /**
     * The currently parsed document
     */
    private QualityQuery[] topic;

    private final String topicPath;

    private final Map<Integer, ArrayList<String>> formulaDictionary = new HashMap<>();

    public TopicParser(String topicPath) {

        if (topicPath == null) {
            throw new NullPointerException("Reader cannot be null.");
        }
        else
            this.topicPath = topicPath;
    }

    public void parseTopic() throws XMLStreamException {

        Map<Integer, QualityQuery> qqMap = new HashMap<>();
        ArrayList<String> formula_IDs = new ArrayList<>();
        int qqMapIndex = 1;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = null;
        try {
             reader = xmlInputFactory.createXMLEventReader(new FileInputStream(topicPath));
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }

        String id = null;
        String title = "";
        String question = "";
        String tags = "";

        boolean openTitle = false;
        boolean openQuestion = false;

        while (reader.hasNext()) {

            Map<String, String> topicAttribute = new HashMap<>();

            XMLEvent nextEvent = reader.nextEvent();

            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "Topic":
                        id = startElement.getAttributeByName(new QName("number")).getValue();
                        break;
                    case "Title":
                        nextEvent = reader.nextEvent();
                        title = title + nextEvent.asCharacters().getData();
                        openTitle = true;
                        break;
                    case "Question":
                        nextEvent = reader.nextEvent();
                        question = question + nextEvent.asCharacters().getData();
                        openQuestion = true;
                        break;
                    case "Tags":
                        nextEvent = reader.nextEvent();
                        tags = nextEvent.asCharacters().getData();
                        break;
                }
            }
            else if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if(endElement.getName().toString()=="Title")
                    openTitle = false;
                if(endElement.getName().toString()=="Question")
                    openQuestion = false;
                if (endElement.getName().getLocalPart().equals("Topic")) {

                    int titleIndex = 0;
                    String pattern = "\"math-container\" id=\"";    //<span class="math-container" id="5640526">  ESEMPIO PATTERN
                    int pattern_length = pattern.length();
                    while(title.indexOf(pattern , titleIndex)>-1)
                    {
                        int startIndex = title.indexOf("\"math-container\" id=\"", titleIndex);
                        //System.out.println("StartIndex Position: " + startIndex);
                        int endIndex = title.indexOf("\"", (startIndex+pattern_length));
                        //System.out.println("EndIndex Position: " + endIndex);
                        //System.out.println("DOCID: " + id);
                        //System.out.println("BODY: " + body.toString());
                        //System.out.println(body.toString().substring(startIndex+pattern_length, endIndex));
                        formula_IDs.add(title.substring(startIndex+pattern_length, endIndex));
                        titleIndex = endIndex;
                    }
                    title = title.replaceAll("<span.*?\\/span>|<[^>]*>", " ");
                    topicAttribute.put("Title", title);

                    int questionIndex = 0;
                    while(question.indexOf(pattern , questionIndex)>-1)
                    {
                        int startIndex = question.indexOf("\"math-container\" id=\"", questionIndex);
                        //System.out.println("StartIndex Position: " + startIndex);
                        int endIndex = question.indexOf("\"", (startIndex+pattern_length));
                        //System.out.println("EndIndex Position: " + endIndex);
                        //System.out.println("DOCID: " + id);
                        //System.out.println("BODY: " + body.toString());
                        //System.out.println(body.toString().substring(startIndex+pattern_length, endIndex));
                        formula_IDs.add(question.substring(startIndex+pattern_length, endIndex));
                        questionIndex = endIndex;
                    }
                    question = question.replaceAll("<span.*?\\/span>|<[^>]*>", " ");
                    topicAttribute.put("Question", question);

                    formulaDictionary.put(qqMapIndex, formula_IDs);

                    topicAttribute.put("Tags", tags);
                    QualityQuery qq = new QualityQuery(id, topicAttribute);
                    qqMap.put(qqMapIndex, qq);
                    qqMapIndex++;
                    id = null;
                    title = "";
                    question = "";
                    tags = "";
                    formula_IDs = new ArrayList<>();
                }
            }
            else if(openTitle){
                title = title + nextEvent.asCharacters().getData();
            }
            else if(openQuestion){
                question = question + nextEvent.asCharacters().getData();
            }

        }

        reader.close();

        topic = new QualityQuery[qqMapIndex-1];

        for(int i=0; i<(qqMapIndex-1); i++)
        {
            topic[i] = qqMap.get(i+1);
        }
    }

    public QualityQuery[] getParsedTopic(){

        return topic;
    }

    public ArrayList<String> get_formula_IDs(int i){

        return formulaDictionary.get(i);
    }

    public void print(int i){

        String qqID = topic[i].getQueryID();
        String qqT = topic[i].getValue("Title");
        String qqQ = topic[i].getValue("Question");
        String qqTags = topic[i].getValue("Tags");
        System.out.printf("ID: %s%n", qqID);
        System.out.printf("TITLE: %s%n", qqT);
        System.out.printf("QUESTION: %s%n", qqQ);
        System.out.printf("TAGS: %s%n", qqTags);
        System.out.printf("FORMULAS: " + get_formula_IDs(i));
    }

    /**
     * IL MAIN SERVE SOLO PER IL TESTING
     */
    public static void main(String[] args) throws Exception {
        System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));
        String topicPath = "../Topics_Task1_2021_V1.1.xml";

        TopicParser p = null;

        try {
            p = new TopicParser(topicPath);
            p.parseTopic();
        } catch (XMLStreamException e) {
            throw new IllegalArgumentException(
                    String.format("Unable to process topic file %s: %s.", topicPath, e.getMessage()), e);
        }

        long start = System.currentTimeMillis();
        for (int i=0; i < 100; i++) {
            System.out.printf("%n%n------------------------------------%n");
            p.print(i);
            System.out.println("\n\n\n");
        }
        System.out.println("Totale topic parsati: " + 100);
        System.out.println("Terminato in : " + ((System.currentTimeMillis()-start)/1000) + " secondi");

    }

}
