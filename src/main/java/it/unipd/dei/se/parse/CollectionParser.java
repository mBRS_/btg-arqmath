/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.parse;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.util.ArrayList;

/**
 * Provides a very basic parser for the TIPSTER corpus (FBIS, FR, FT, LATIMES), i.e. TREC Disks 4 and 5 minus
 * Congressional Record.
 * <p>
 * It is based on the parser <a href="https://github.com/isoboroff/trec-demo/blob/master/src/TrecDocIterator.java"
 * target="_blank">TrecDocIterator.java</a> by Ian Soboroff.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class CollectionParser extends it.unipd.dei.se.parse.DocumentParser {

    /**
     * The size of the buffer for the body element.
     */
    private static final int BODY_SIZE = 1024 * 8;

    /**
     * The currently parsed document
     */
    private it.unipd.dei.se.parse.ParsedDocument document = null;


    /**
     * Creates a new TIPSTER Corpus document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     * @throws NullPointerException     if {@code in} is {@code null}.
     * @throws IllegalArgumentException if any error occurs while creating the parser.
     */
    public CollectionParser(final Reader in) {
        super(new BufferedReader(in));
    }
    @Override
    public boolean hasNext() {
        try {
            StringWriter body = new StringWriter();
            String id = null;
            String score = null;
            String viewCount = null;
            String title = null;
            String tags = null;
            String postType = null;
            String favouriteCount = null;
            ArrayList<Integer> formula_IDs = new ArrayList<>();

            if (!in.hasNext()) {
                next = false;
            }
            while (in.hasNext()) {
                XMLEvent nextEvent = in.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("row") &&
                            startElement.getAttributeByName(new QName("PostTypeId")).getValue().equals("2")) {
                        id = startElement.getAttributeByName(new QName("Id")).getValue();
                        body.append(startElement.getAttributeByName(new QName("Body")).getValue());
                        int bodyIndex = 0;
                        String pattern = "\"math-container\" id=\"";    //<span class="math-container" id="5640526">  ESEMPIO PATTERN
                        int pattern_length = pattern.length();
                        while(body.toString().indexOf(pattern , bodyIndex)>-1)
                        {
                            int startIndex = body.toString().indexOf("\"math-container\" id=\"", bodyIndex);
                            //System.out.println("StartIndex Position: " + startIndex);
                            int endIndex = body.toString().indexOf("\"", (startIndex+pattern_length));
                            //System.out.println("EndIndex Position: " + endIndex);
                            //System.out.println("DOCID: " + id);
                            //System.out.println("BODY: " + body.toString());
                            //System.out.println(body.toString().substring(startIndex+pattern_length, endIndex));
                            formula_IDs.add(Integer.parseInt(body.toString().substring(startIndex+pattern_length, endIndex)));
                            bodyIndex = endIndex;
                        }
                        score = startElement.getAttributeByName(new QName("Score")).getValue();
                        postType = startElement.getAttributeByName(new QName("PostTypeId")).getValue();
                        if(postType.equals("1")) {
                            viewCount = startElement.getAttributeByName(new QName("ViewCount")).getValue();
                            title = startElement.getAttributeByName(new QName("Title")).getValue();
                            tags = startElement.getAttributeByName(new QName("Tags")).getValue();
                        }
                        // favouriteCount = startElement.getAttributeByName(new QName("FavouriteCount")).getValue();
                    }
                }
                else if (nextEvent.isEndElement()){
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("row") && id!=null) {
                        break;
                    }
                }
            }
            if (id != null) {
                document = new ParsedDocument(id, body.toString().length() > 0 ?
                        body.toString().replaceAll("<[^>]*>", " ") : "#",
                        //body.toString().replaceAll("<[^>]*>", " ") : "#",
                        score,
                        viewCount,
                        title,
                        tags,
                        postType,
                        favouriteCount,
                        formula_IDs);
            } else {
                // entra solo se è nell'ultima iterazione
                document = new ParsedDocument("-1", " ", "0", "0", " ", " ",
                        " ", "0", formula_IDs);
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return next;
    }

    @Override
    protected final it.unipd.dei.se.parse.ParsedDocument parse() {
        return document;
    }


    /**
     * Main method of the class. Just for testing purposes.
     *
     * @param args command line arguments.
     * @throws Exception if something goes wrong while indexing.
     */
    public static void main(String[] args) throws Exception {
        System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));
        Reader reader = new FileReader(
                "../Posts/Posts_2020_V1.1.xml");

        CollectionParser p = new CollectionParser(reader);
        int counter = 0;
        long start = System.currentTimeMillis();
        System.out.println("Parsing dei documenti: ");
        for (it.unipd.dei.se.parse.ParsedDocument d : p) {
            System.out.printf("%n%n------------------------------------%n%s%n%n%n", d.toString());
            counter++;
            if(counter%10000 == 0)
                System.out.println(counter + "  documenti parsati");
        }
        System.out.println("Totale documenti parsati: " + counter);
        System.out.println("Terminato in : " + ((System.currentTimeMillis()-start)/1000) + " secondi");

    }

}
