import csv
import re
import os


#
def findLongestRow(rows):
    title = []
    question = []

    for row in rows:
        if row['type'] == "title":
            title.append(row)
        else:
            question.append(row)
    
    if len(title) != 0:
        longest_row = title[0]
        for row in title:
            if len(row['formula']) > len(longest_row['formula']):
                longest_row = row
        return longest_row

    else:
        longest_row = question[0]
        for row in question:
            if len(row['formula']) > len(longest_row['formula']):
                longest_row = row
        return longest_row


#
def searchFormula(tsv_file, id_row):
    # Read every line of tsv file and parsing
    with open(tsv_file, encoding="utf-8") as TSVFile:
        # TSV file with header
        spamreader = csv.DictReader(TSVFile, delimiter="\t")

        for row in spamreader:
            if row['id'] == id_row:
                return row['formula']


#
def printFormula(formula, isOPT):
    if isOPT:
        return formula[75:-7]
    else:
        return re.split("<semantics>|</semantics>", formula)[1]


#
def create_xml(tsv_file, xml_file, latex_file, isOPT):
    # Remove old output file
    if os.path.exists(xml_file + ".xml"):
        os.remove(xml_file + ".xml")

    # Read every line of tsv file and parsing
    with open(latex_file, encoding = "utf-8") as TSVFile:
        # TSV file with header
        spamreader = csv.DictReader(TSVFile, delimiter = "\t")

        last_topic_id = 0
        all_rows = []
        rows = []

        for row in spamreader:
            # For each topic_id, create a list of all rows that belong to the same topic_id
            # and append that list to another list
            if row['topic_id'] == last_topic_id:
                rows.append(row)

            if row['topic_id'] != last_topic_id:
                last_topic_id = row['topic_id']
                all_rows.append(rows)
                rows = []
                rows.append(row)
        
        all_rows.append(rows)

    # Delete the first empty list
    del all_rows[0]

    # Create "topics" tag for the XML file
    XMLFile = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
    XMLFile += "<topics>\n"

    # Populate XML file
    for rows in all_rows:
        longest_row = findLongestRow(rows)

        XMLFile += "\t<topic>\n"
        XMLFile += "\t\t<num>" + longest_row['topic_id'] + "</num>\n"
        XMLFile += "\t\t<query>\n"
        XMLFile += "\t\t\t<formula id=\"" + longest_row['id'] + "\">\n"

        XMLFile += "\t\t\t\t<math alttext=\"" + longest_row['formula'] + "\" display=\"block\">"
        XMLFile += printFormula(searchFormula(tsv_file, longest_row['id']), isOPT)
        XMLFile += "</math>\n"

        XMLFile += "\t\t\t</formula>\n"
        XMLFile += "\t\t</query>\n"
        XMLFile += "\t</topic>\n"

    # Write XML file
    XMLFile += "</topics>\n"
    with open(xml_file + ".txt", 'w', encoding = "utf-8") as writer:
        writer.write(XMLFile)
    os.rename(xml_file + ".txt", xml_file + '.xml')

        




# Define file names
latex_file = "Topics_2021_Formulas_Latex_V1.1.tsv"

opt_tsv_file = "Topics_2021_Formulas_OPT_V1.1.tsv"
slt_tsv_file = "Topics_2021_Formulas_SLT_V1.1.tsv"

opt_xml_file = "opt_task1_inv"
slt_xml_file = "slt_task1_inv"


# Create the XML files (without math "alttext" tags)
create_xml(opt_tsv_file, opt_xml_file, latex_file, True)
create_xml(slt_tsv_file, slt_xml_file, latex_file, False)
